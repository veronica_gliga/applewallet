import sys, os.path, hashlib, re
import zipfile
import subprocess
from StringIO import StringIO
from io import BytesIO

#
# 1. Go to provisioning profile, create a new Pass Type ID
# 2. Download genereated pass.cer file and import into Keychain
# 3. Export as Certificates.p12
# 4. Split out key and cert:
#    openssl pkcs12 -in Certificates.p12 -clcerts -nokeys -out passcert.pem
#    openssl pkcs12 -in Certificates.p12 -nocerts -out passkey.pem
# 5. Download Apple WWDR certificate:
#    https://developer.apple.com/certificationauthority/AppleWWDRCA.cer
#    Export from Keychain to wwdr.pem
# 6. Put supporting files in a new folder called "components"
#    logo.png, logo@2x.png, thumbnail.png, thumbnail@2x.png, etc.
# 7. Move the .pem files into components
# 8. Create components/pass.json with the contents of the passbook pass
# 9. Run the script. Cross your fingers.
#

# For detailed Passbook documentation, see the official Apple docs:
#   https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/PassKit_PG/Chapters/Introduction.html

#
# to hash the manifest:
#   shasum manifest
# to manually parse the signature:
#   openssl asn1parse -in signature -inform der
#   you should see the shasum in there somewhere (under :messageDigest)
# to manually verify the signature:
#   openssl smime -verify -in signature -content manifest.json -inform der -noverify
#   the "-noverify" means to not verify the entire CA chain, just check the
#      message signature vs the content, using the certs embeeded in signature
#

APP_MEDIA_DIR = './components/'

WWDR_FILENAME = APP_MEDIA_DIR + 'wwdr.pem'
PASS_KEY_FILENAME = APP_MEDIA_DIR + 'passkey.pem'  
PASS_CERT_FILENAME = APP_MEDIA_DIR + 'passcert.pem' 

files = ('pass.json', 'logo.png', 'logo@2x.png', 'icon.png', 'icon@2x.png', 
    'thumbnail.png', 'thumbnail@2x.png')


#
# load supporting files, hash 'em, build up manifest
#
file_data = {}
file_hashes = {}
manifest = '{\n'
for file in files:
    f = open(APP_MEDIA_DIR + file, 'r')
    data = f.read()
    f.close()
    filename = os.path.basename(file)
    file_data[filename] = data
    file_hashes[filename] = hashlib.sha1(data).hexdigest()
    manifest += '   "%s":"%s",\n' % (filename, file_hashes[filename])

manifest += '}\n'

# 
# write the manifest to manifest.json
#
f = open(APP_MEDIA_DIR + 'manifest.json', 'w')
f.write(manifest)
f.close()

#
# sign the manifest, save as a detatched binary DER signature in "signature"
#
cmd = '/usr/bin/openssl smime -sign -signer %s -inkey %s -certfile %s -in %smanifest.json -out %ssignature -outform der -binary' % (PASS_CERT_FILENAME, PASS_KEY_FILENAME, WWDR_FILENAME, APP_MEDIA_DIR, APP_MEDIA_DIR )
print(cmd)
subprocess.call(cmd, shell=True)

# 
# add manifest and signature to the list of files
#
for file in ['manifest.json', 'signature']:
    f = open(APP_MEDIA_DIR + file, 'r')
    data = f.read()
    f.close()
    filename = os.path.basename(file)
    file_data[filename] = data
    file_hashes[filename] = hashlib.sha1(data).hexdigest()

#
# put all the files into a zip file
##

zipObj = zipfile.ZipFile('pass.pkpass', 'w', allowZip64 = True)
for filename, data in file_data.items():
    print(filename)
    zipObj.writestr(filename, data)

zipObj.close()

#
# send emmail with the newly created pass
#

import email, smtplib, ssl

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from getpass import getpass

subject = "An email with attachment from Python"
body = "This is an email with attachment sent from Python"
sender_email = input("Introduce your email: ")
receiver_email = input("Introduce recipient email: ")
password = getpass()

# Create a multipart message and set headers
message = MIMEMultipart()
message["From"] = sender_email
message["To"] = receiver_email
message["Subject"] = subject

# Add body to email
message.attach(MIMEText(body, "plain"))

# Make sure it is in same directory as script
filename = "pass.pkpass"

# Open .pkpass file in application/vnd.apple.pkpass mode
with open(filename, "rb") as attachment:
    # Add file as application/vnd.apple.pkpass
    part = MIMEBase("application", "vnd.apple.pkpass")
    part.set_payload(attachment.read())

# Encode file in ASCII characters to send by email
encoders.encode_base64(part)

# Add header as key/value pair to attachment part
part.add_header(
    "Content-Disposition",
    "attachment; filename= pass.pkpass",
)

# Add attachment to message and convert message to string
message.attach(part)
text = message.as_string()

# Log in to server using secure context and send email
server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
server.login(sender_email, password)
server.sendmail(sender_email, receiver_email, text)
